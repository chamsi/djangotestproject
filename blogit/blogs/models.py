from django.db import models

# Create your models here.
class Theme(models.Model):
    IdTheme = models.AutoField(primary_key=True)  
    categorie = models.CharField(max_length=255,null=False)
    def __str__(self):
       return self.categorie

class Article(models.Model):  
    id = models.AutoField(primary_key=True)  
    Title = models.CharField(max_length=1000,null=False)  
    Image = models.CharField(max_length=255,null=False)  
    HeaderImage = models.CharField(max_length=255,null=False)  
    Introduction = models.CharField(max_length=2000,null=False)
    Description = models.CharField(max_length=2000,null=False)
    LastMod = models.DateTimeField(auto_now_add=True) 
    Language = models.CharField(max_length=4,null=False)  
    KeyWords = models.CharField(max_length=1000,null=False)  
    State = models.IntegerField(null=False)   
    NumVisit = models.CharField(max_length=100,null=False)
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE,db_column='IdTheme')                                      
    IdUser = models.IntegerField(null=False)
    IdHost = models.IntegerField(null=False)
    def __str__(self):
       return self.Title  