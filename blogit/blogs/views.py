from django.shortcuts import render

from blogs.models import Article


# Create your views here.
def index(request):
	shelf = Article.objects.all()
	return render(request, 'article/index.html', {'shelf': shelf})
def whoweare1(request):
	
	return render(request, 'article/whoweare1.html')
def whoweare2(request):
	
	return render(request, 'article/whoweare2.html')