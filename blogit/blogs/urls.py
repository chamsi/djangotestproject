from . import views
from django.urls import path
from django.conf.urls.static import static



from blogit.settings import  STATIC_ROOT, STATIC_URL


urlpatterns = [
	path('', views.index, name = 'index'),
    path('whoweare1/', views.whoweare1, name = 'whoweare1'),
    path('whoweare2/', views.whoweare2, name = 'whoweare2'),
]


urlpatterns += static(STATIC_URL, document_root = STATIC_ROOT)
