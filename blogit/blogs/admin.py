from django.contrib import admin

from blogs.models import Article, Theme

# Register your models here.
admin.site.register(Article)
admin.site.register(Theme)